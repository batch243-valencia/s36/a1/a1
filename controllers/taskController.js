const task = require("../models/task")
const Task=require("../models/task")



// FOR DEBUG PURPOSE ONLY
module.exports.getAllTasks=()=>{
    return Task.find({}).then(result=>{
        return result
    })

}

// FOR DEBUG PURPOSE ONLY
		module.exports.createTask = (requestBody) =>{

			//Creates a task object based on the Mongoose model "Task"
			let newTask = new Task({

				//Sets the "name" property with the value receive from the client/Postman
				name: requestBody.name
			})
		

		//Save the newly created "newTask" object in the MongoDB database

				 return newTask.save().then((task,error) =>{

				 	if(error){
				 		console.log(error);

				 		return false;
				 	} else {

				 		return task;
				 	}
				 })
			}


module.exports.findTaskByID = (id) => {
            try{
                return Task.findById(id);
            }catch(err){
                console.log(err);
                return false;
            }
        }


module.exports.updateTask=(taskId, newContent)=>{
    return Task.findById(taskId).then((result,error)=>{
        if(error){
            console.log(error)
            return false
        }
        result.status="Complete";
        return result.save().then((updatedTask,saveErr)=>{
            if(saveErr){
                console.log(saveErr)
                return false
            }else{
                return updatedTask
            }
        })})}

