const express = require('express');
const mongoose = require('mongoose');


const app = express();
const port = 3001;

const taskRoute = require("./routes/taskRoute");

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/tasks", taskRoute)

//database s36-B243-to-do
mongoose.connect("mongodb+srv://admin:admin@batch243-valencia.hpmv0an.mongodb.net/s36-Activity?retryWrites=true&w=majority",	{ 
				  useNewUrlParser:true,
				  useUnifiedTopology: true
				});
app.listen(port,() =>console.log(`Server running at port ${port}`));

